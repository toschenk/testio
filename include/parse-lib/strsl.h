#pragma once

#define PRIstrsl "%.*s"
#define PRIARGstrsl(X) (int)((X).len), (X).data

typedef struct strsl strsl_t;

struct strsl {
	size_t len;
	const char *data;
};

static inline strsl_t strsl_null(void)
{
	return (strsl_t){ 0, NULL };
}

static inline strsl_t strsl_smk(
		const char *s)
{
	if(s)
		return (strsl_t){ strlen(s), s };
	else
		return (strsl_t){ 0, NULL };
}

static inline strsl_t strsl_hexstr(
		char *buf, /* must be twice the size of 'size' */
		const void *data,
		size_t size)
{
	char *di = buf;
	const char *si = (const char*)data;
	for(size_t i = 0; i < size; i++) {
		char h = (*si >> 4) & 0x0f;
		char l = *si & 0x0f;
		if(h < 0x0a)
			h += '0';
		else
			h += 'a' - 0x0a;
		if(l < 0x0a)
			l += '0';
		else
			l += 'a' - 0x0a;
		*di++ = h;
		*di++ = l;
		si++;
	}
	return (strsl_t){ size * 2, buf };
}

static inline strsl_t strsl_snmk(
		const char *s,
		size_t len)
{
	return (strsl_t){ strnlen(s, len), s };
}

static inline strsl_t strsl_snmk_full(
		const char *s,
		size_t len)
{
	return (strsl_t){ len, s };
}

/* return non-const pointer to string, given the buffer where the slice is located. */
static inline char *strsl_str(
		strsl_t s,
		char *str)
{
	return str + (s.data - str);
}

/* CAUTION: only use if string is known to contain enough chars. */
static inline void strsl_rewind(
		strsl_t *s,
		size_t n)
{
	s->data -= n;
	s->len += n;
}

static inline void strsl_trim(
		strsl_t *s)
{
	while(s->len && ischar_spc(*s->data)) {
		s->data++;
		s->len--;
	}
	while(s->len && ischar_spc(s->data[s->len - 1]))
		s->len--;
}

static inline void strsl_rtrim(
		strsl_t *s)
{
	while(s->len && ischar_spc(s->data[s->len - 1]))
		s->len--;
}

static inline void strsl_ltrim(
		strsl_t *s)
{
	while(s->len && ischar_spc(*s->data)) {
		s->data++;
		s->len--;
	}
}

static inline void strsl_setchar(
		strsl_t s,
		char *str,
		size_t index,
		char c)
{
	if(index < s.len)
		str[s.data - str + index] = c;
}

static inline void strsl_tolower(
		strsl_t s,
		char *str) /* pointer to buffer, where 's' is located. this is required since s holds a const char pointer. */
{
	const char *end = s.data + s.len;
	for(char *c = str + (s.data - str); c != end; c++)
		if(*c >= 'A' && *c <= 'Z') {
			*c -= 'A';
			*c += 'a';
		}
}

static inline void strsl_toupper(
		strsl_t s,
		char *str) /* pointer to buffer, where 's' is located. this is required since s holds a const char pointer. */
{
	const char *end = s.data + s.len;
	for(char *c = str + (s.data - str); c != end; c++)
		if(*c >= 'a' && *c <= 'z') {
			*c -= 'a';
			*c += 'A';
		}
}

/* left-shift character from post to pre.
 * initializes pre if its length is 0.
 * operation: [ 1234 ] [ 5678 ] -> [ 12345 ] [ 678 ]
 * shifting effectively results in a split string;
 * concatenating dest + src yields the original string.
 * function does not copy anything, it just updates slices, so that dest + src. */
static inline bool strsl_lshift(
		strsl_t *pre,
		strsl_t *post)
{
	if(post->len == 0)
		return false;
	else if(pre->len == 0)
		pre->data = post->data;
	else {
		assert(pre->data == post->data - pre->len); /* shift: just move boundary from post to pre while referencing the original allocated string */
	}
	pre->len++;
	post->len--;
	post->data++;
	return true;
}

static inline bool strsl_rshift(
		strsl_t *pre,
		strsl_t *post)
{
	if(pre->len == 0)
		return false;
	else if(post->len == 0)
		post->data = pre->data;
	else {
		assert(pre->data == post->data - pre->len); /* shift: just move boundary from post to pre while referencing the original allocated string */
	}
	pre->len--;
	post->len++;
	post->data--;
	return true;
}

/* front chop */
static inline char strsl_fchop(
		strsl_t *s)
{
	if(s->len > 0) {
		s->len--;
		return *s->data++;
	}
	else
		return 0;
}

static inline char strsl_bchop(
		strsl_t *s)
{
	if(s->len > 0)
		return s->data[--s->len];
	else
		return 0;
}

static inline int strsl_cmp(
		strsl_t a,
		strsl_t b)
{
	int cmp = memcmp(a.data, b.data, MIN(a.len, b.len));
	if(cmp)
		return cmp;
	else if(a.len < b.len)
		return -1;
	else if(a.len > b.len)
		return 1;
	else
		return 0;
}

static inline int strsl_scmp(
		strsl_t a,
		const char *b)
{
	size_t len;
	int cmp;
	if(b)
		len = strlen(b);
	else
		len = 0;
	cmp = memcmp(a.data, b, MIN(a.len, len));
	if(cmp)
		return cmp;
	else if(a.len < len)
		return -1;
	else if(a.len > len)
		return 1;
	else
		return 0;
}

static inline int strsl_scasecmp(
		strsl_t a,
		const char *b)
{
	size_t len;
	size_t blen;
	if(b)
		blen = strlen(b);
	else
		blen = 0;
	len = MIN(a.len, blen);
	for(size_t i = 0; i < len; i++) {
		char ca = a.data[i];
		char cb = b[i];
		if(ca >= 'A' && ca <= 'Z') {
			ca -= 'A';
			ca += 'a';
		}
		if(cb >= 'A' && cb <= 'Z') {
			cb -= 'A';
			cb += 'a';
		}
		if(ca < cb)
			return -1;
		else if(ca > cb)
			return 1;
	}
	if(a.len < blen)
		return -1;
	else if(a.len > blen)
		return 1;
	else
		return 0;
}

/* compare slice with c string at given position.
 * characters beyound slice boundaries are treated as 0. */
static inline int strsl_scmpsub(
		strsl_t s,
		size_t pos,
		const char *str)
{
	size_t len;
	if(!str)
		return 0;
	len = strlen(str);
	if(len == 0)
		return 0;
	else if(pos > s.len)
		return 1;
	else if(len > s.len - pos) {
		int cmp = memcmp(s.data + pos, str, s.len - pos);
		if(cmp)
			return cmp;
		/* check, whether remaining part of 'str' is all zero */
		for(size_t i = s.len - pos; i < len; i++)
			if(str[i])
				return 1;
		return 0;
	}
	else
		return memcmp(s.data + pos, str, len);
}

/* compare slice with c string at given position.
 * characters beyound slice boundaries are treated as 0. */
static inline int strsl_sncmpsub(
		strsl_t s,
		size_t pos,
		const char *str,
		size_t len)
{
	if(pos > s.len) {
		/* check, whether 'str' is all zero */
		for(size_t i = 0; i < len; i++)
			if(str[i])
				return 1;
		return 0;
	}
	else if(len > s.len - pos) {
		int cmp = memcmp(s.data + pos, str, s.len - pos);
		if(cmp)
			return cmp;
		/* check, whether remaining part of 'str' is all zero */
		for(size_t i = s.len - pos; i < len; i++)
			if(str[i])
				return 1;
		return 0;
	}
	else
		return memcmp(s.data + pos, str, len);
}

static inline char *strsl_dup(
		strsl_t s)
{
	char *str = (char*)malloc(s.len + 1);
	memcpy(str, s.data, s.len);
	str[s.len] = 0;
	return str;
}


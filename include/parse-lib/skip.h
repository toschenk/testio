#pragma once

#include <stdint.h>

static inline bool skip_spc(
		strsl_t *s)
{
	bool found = false;
	while(s->len > 0 && ischar_spc(*s->data)) {
		found = true;
		s->len--;
		s->data++;
	}
	return found;
}

static inline bool skip_spcr(
		strsl_t *ps,
		strsl_t *spc)
{
	strsl_t s = *ps;
	while(ps->len > 0 && ischar_spc(*ps->data)) {
		ps->len--;
		ps->data++;
	}
	if(spc) {
		spc->data = s.data;
		spc->len = s.len - ps->len;
	}
	return ps->len < s.len;
}

static inline bool skip_spcl(
		strsl_t *s)
{
	bool found = false;
	while(s->len > 0 && ischar_spcl(*s->data)) {
		found = true;
		s->len--;
		s->data++;
	}
	return found;
}

static inline void skip_line(
		strsl_t *s,
		strsl_t *val)
{
	if(val)
		val->data = s->data;
	while(s->len > 0 && !ischar_newline(*s->data)) {
		s->len--;
		s->data++;
	}
	if(val)
		val->len = s->data - val->data;
}

static inline bool skip_nl(
		strsl_t *s)
{
	bool found = false;
	while(s->len > 0 && ischar_newline(*s->data)) {
		found = true;
		s->len--;
		s->data++;
	}
	return found;
}

static inline bool skip_char(
		strsl_t *s,
		char c)
{
	if(s->len && *s->data == c) {
		s->len--;
		s->data++;
		return true;
	}
	else
		return false;
}

static inline size_t skip_all(
		strsl_t *s,
		char c)
{
	size_t n = 0;
	while(s->len && *s->data == c) {
		s->len--;
		s->data++;
		n++;
	}
	return n;
}

static inline bool skip_crange(
		strsl_t *s,
		char from,
		char to,
		char *c)
{
	if(s->len == 0)
		return false;
	else if(*s->data >= from && *s->data <= to) {
		if(c)
			*c = *s->data;
		s->data++;
		s->len--;
		return true;
	}
	else
		return false;
}

static inline bool skip_fn(
		strsl_t *s,
		bool (*ischar)(char c),
		char *c)
{
	if(s->len && ischar(*s->data)) {
		if(c)
			*c = *s->data;
		s->len--;
		s->data++;
		return true;
	}
	else
		return false;
}

static inline bool skip_tok(
		strsl_t *s,
		char c,
		strsl_t *value)
{
	strsl_t org = *s;
	while(s->len && *s->data != c) {
		s->data++;
		s->len--;
	}
	if(s->len) {
		if(value) {
			*value = org;
			value->len -= s->len;
		}
		s->len--;
		s->data++;
		return true;
	}
	else {
		*s = org;
		return false;
	}
}

static inline char skip_fntok(
		strsl_t *s,
		bool (*istok)(char c),
		strsl_t *value)
{
	strsl_t org = *s;
	while(s->len && !istok(*s->data)) {
		s->data++;
		s->len--;
	}
	if(s->len) {
		char c = *s->data;
		if(value) {
			*value = org;
			value->len -= s->len;
		}
		s->len--;
		s->data++;
		return c;
	}
	else {
		*s = org;
		return 0;
	}
}

static inline bool skip_toksl(
		strsl_t *s,
		strsl_t tok,
		strsl_t *value)
{
	strsl_t org = *s;
	if(tok.len == 0)
		return false;
	while(s->len >= tok.len && memcmp(s->data, tok.data, tok.len)) { //TODO improved algorithm
		s->data++;
		s->len--;
	}
	if(s->len >= tok.len) {
		char c = *s->data;
		if(value) {
			*value = org;
			value->len -= s->len;
		}
		s->len -= tok.len;
		s->data += tok.len;
		return true;
	}
	else {
		*s = org;
		return false;
	}
}

static inline bool skip_strsl(
		strsl_t *s,
		strsl_t test)
{
	if(s->len >= test.len && !memcmp(s->data, test.data, test.len)) {
		s->data += test.len;
		s->len -= test.len;
		return true;
	}
	else
		return false;
}

static inline bool skip_str(
		strsl_t *s,
		const char *str)
{
	size_t len = strlen(str);
	if(len <= s->len && !strncmp(s->data, str, len)) {
		s->len -= len;
		s->data += len;
		return true;
	}
	else
		return false;
}

static inline unsigned int skip_sopt(
		strsl_t *s,
		...)
{
	va_list ap;
	va_start(ap, s);
	for(unsigned int opt = 1;; opt++) {
		size_t len;
		const char *str = va_arg(ap, const char*);
		if(!str) {
			va_end(ap);
			return 0;
		}
		len = strlen(str);
		if(!strsl_sncmpsub(*s, 0, str, len)) {
			va_end(ap);
			s->data += len;
			s->len -= len;
			return opt;
		}
	}
}

static inline unsigned int skip_sropt(
		strsl_t *s,
		strsl_t *value,
		...)
{
	va_list ap;
	va_start(ap, value);
	for(unsigned int opt = 1;; opt++) {
		size_t len;
		const char *str = va_arg(ap, const char*);
		if(!str) {
			va_end(ap);
			return 0;
		}
		len = strlen(str);
		if(!strsl_sncmpsub(*s, 0, str, len)) {
			va_end(ap);
			if(value) {
				*value = *s;
				value->len = len;
			}
			s->data += len;
			s->len -= len;
			return opt;
		}
	}
}

/* returns: 
 * 0: no match;
 * > 0: argument index, starting at 1 */
static inline unsigned int skip_copt(
		strsl_t *s,
		...)
{
	va_list ap;
	if(s->len == 0)
		return 0;
	va_start(ap, s);
	for(unsigned int opt = 1;; opt++) {
		int c = va_arg(ap, int);
		if(!c) {
			va_end(ap);
			return 0;
		}

		if(*s->data == c) {
			va_end(ap);
			s->data++;
			s->len--;
			return opt;
		}
	}
}

/* returns: 
 * 0: no match;
 * > 0: argument index, starting at 1 */
static inline unsigned int skip_cropt(
		strsl_t *s,
		char *r,
		...)
{
	va_list ap;
	if(s->len == 0)
		return 0;
	va_start(ap, r);
	for(unsigned int opt = 1;; opt++) {
		int c = va_arg(ap, int);
		if(!c) {
			va_end(ap);
			return 0;
		}

		if(*s->data == c) {
			va_end(ap);
			s->data++;
			s->len--;
			if(r)
				*r = c;
			return opt;
		}
	}
}

static inline bool skip_fnquoted(
		strsl_t *ps,
		bool (*is_delim)(char),
		char escape,
		strsl_t *value,
		bool *has_escape)
{
	if(has_escape)
		*has_escape = false;
	for(strsl_t s = *ps; s.len;) {
		if(is_delim(*s.data)) {
			if(value) {
				value->data = ps->data;
				value->len = ps->len - s.len;
			}
			s.len--;
			s.data++;
			*ps = s;
			return true;
		}
		else if(*s.data == escape) {
			s.len--;
			s.data++;
			if(has_escape)
				*has_escape = true;
			if(!s.len)
				return false;
		}
		s.len--;
		s.data++;
	}
	if(is_delim(0)) {
		if(value) {
			value->data = ps->data;
			value->len = ps->len;
		}
		ps->data += ps->len;
		ps->len = 0;
		return true;
	}
	else
		return false;
}

static inline bool skip_quoted(
		strsl_t *ps,
		char delim,
		char escape,
		strsl_t *value,
		bool *has_escape)
{
	if(has_escape)
		*has_escape = false;
	for(strsl_t s = *ps; s.len;) {
		if(*s.data == delim) {
			if(value) {
				value->data = ps->data;
				value->len = ps->len - s.len;
			}
			s.len--;
			s.data++;
			*ps = s;
			return true;
		}
		else if(*s.data == escape) {
			s.len--;
			s.data++;
			if(has_escape)
				*has_escape = true;
			if(!s.len)
				return false;
		}
		s.len--;
		s.data++;
	}
	return false;
}

static inline bool skip_fnseq0(
		strsl_t *s,
		bool (*ischar0)(char c),
		bool (*ischar)(char c),
		strsl_t *seq)
{
	if(!s->len || !ischar0(*s->data))
		return false;
	if(seq)
		seq->data = s->data;
	s->data++;
	s->len--;
	while(s->len && ischar(*s->data)) {
		s->data++;
		s->len--;
	}
	if(seq)
		seq->len = s->data - seq->data;
	return true;
}

static inline bool skip_fnseq(
		strsl_t *s,
		bool (*ischar)(char c),
		strsl_t *seq)
{
	if(!s->len || !ischar(*s->data))
		return false;
	if(seq)
		seq->data = s->data;
	s->data++;
	s->len--;
	while(s->len && ischar(*s->data)) {
		s->data++;
		s->len--;
	}
	if(seq)
		seq->len = s->data - seq->data;
	return true;
}

static inline bool skip_ccat(
		strsl_t *s,
		const unsigned char *cats,
		unsigned char cat,
		bool high,
		char *c)
{
	if(s->len && ((*s->data < 0 && high) || (*s->data >= 0 && (cats[(unsigned char)*s->data] & cat)))) {
		s->len--;
		s->data++;
		if(c)
			*c = *s->data;
		return true;
	}
	else
		return false;
}

static inline bool skip_cseq0(
		strsl_t *s,
		const unsigned char *cats, /* size: 128; for each character < 128: bitmask of categories */
		unsigned char cat0, /* first character: bitmask for accepted categories */
		bool high0, /* first character: accept chars >= 128 */
		unsigned char cat, /* bitmask for accepted categories */
		bool high, /* accept chars >= 128 */
		strsl_t *seq)
{
	if(!s->len || (*s->data < 0 && !high0) || (*s->data >= 0 && !(cats[(unsigned char)*s->data] & cat0)))
		return false;
	if(seq)
		seq->data = s->data;
	s->data++;
	s->len--;
	while(s->len && ((*s->data < 0 && high) || (*s->data >= 0 && (cats[(unsigned char)*s->data] & cat)))) {
		s->data++;
		s->len--;
	}
	if(seq)
		seq->len = s->data - seq->data;
	return true;
}

static inline bool skip_cseq(
		strsl_t *s,
		const unsigned char *cats, /* size: 128; for each character < 128: bitmask of categories */
		unsigned char cat, /* bitmask for accepted categories */
		bool high, /* accept chars >= 128 */
		strsl_t *seq)
{
	strsl_t s2 = *s;
	if(!s->len)
		return false;
	if(seq)
		seq->data = s->data;
	while(s->len && ((*s->data < 0 && high) || (*s->data >= 0 && (cats[(unsigned char)*s->data] & cat)))) {
		s->data++;
		s->len--;
	}
	if(seq)
		seq->len = s->data - seq->data;
	return s->len < s2.len;
}

/* NOTE: skip_c64seq are the same as skip_cseq, except that the per-char bitmask uses 64 bits instead of 8 bits */

static inline bool skip_c64seq0(
		strsl_t *s,
		const uint64_t *cats, /* size: 128; for each character < 128: bitmask of categories */
		uint64_t cat0, /* first character: bitmask for accepted categories */
		bool high0, /* first character: accept chars >= 128 */
		uint64_t cat, /* bitmask for accepted categories */
		bool high, /* accept chars >= 128 */
		strsl_t *seq)
{
	if(!s->len || (*s->data < 0 && !high0) || (*s->data >= 0 && !(cats[(unsigned char)*s->data] & cat0)))
		return false;
	if(seq)
		seq->data = s->data;
	s->data++;
	s->len--;
	while(s->len && ((*s->data < 0 && high) || (*s->data >= 0 && (cats[(unsigned char)*s->data] & cat)))) {
		s->data++;
		s->len--;
	}
	if(seq)
		seq->len = s->data - seq->data;
	return true;
}

static inline bool skip_c64seq(
		strsl_t *s,
		const uint64_t *cats, /* size: 128; for each character < 128: bitmask of categories */
		uint64_t cat, /* bitmask for accepted categories */
		bool high, /* accept chars >= 128 */
		strsl_t *seq)
{
	strsl_t s2 = *s;
	if(!s->len)
		return false;
	if(seq)
		seq->data = s->data;
	while(s->len && ((*s->data < 0 && high) || (*s->data >= 0 && (cats[(unsigned char)*s->data] & cat)))) {
		s->data++;
		s->len--;
	}
	if(seq)
		seq->len = s->data - seq->data;
	return s->len < s2.len;
}

static inline bool skip_int(
		strsl_t *s,
		strsl_t *value)
{
	strsl_t org = *s;
	if(!s->len)
		return false;
	else if(*s->data == '-' || *s->data == '+') {
		if(s->len == 1)
			return false;
		s->len--;
		s->data++;
	}
	if(*s->data == '0') {
		s->len--;
		s->data++;
		if(s->len && *s->data >= '0' && *s->data <= '9')
			goto nomatch;
	}
	else if(*s->data < '1' || *s->data > '9')
		goto nomatch;
	else {
		s->len--;
		s->data++;
	}
	while(s->len && *s->data >= '0' && *s->data <= '9') {
		s->len--;
		s->data++;
	}
	if(value) {
		*value = org;
		value->len -= s->len;
	}
	return true;
nomatch:
	*s = org;
	return false;
}

static inline bool skip_val_size(
		strsl_t *s,
		size_t *pval)
{
	strsl_t org = *s;
	strsl_t sval;
	size_t val = 0;
	if(!skip_int(s, &sval))
		return false;
	else if(sval.data[0] == '-') {
		*s = org;
		return false;
	}
	else if(sval.data[0] == '+') {
		sval.len--;
		sval.data++;
	}

	while(sval.len) {
		size_t old = val;
		val *= 10;
		val += sval.data[0] - '0';
		if(val < old) {
			*s = org;
			return false;
		}
		sval.len--;
		sval.data++;
	}

	if(pval)
		*pval = val;
	return true;
}

static inline bool skip_val_s64(
		strsl_t *s,
		int64_t *pval)
{
	strsl_t org = *s;
	strsl_t sval;
	uint64_t val = 0;
	int64_t sign = 1;
	if(!skip_int(s, &sval))
		return false;
	else if(sval.data[0] == '-') {
		sign = -1;
		sval.len--;
		sval.data++;
	}
	else if(sval.data[0] == '+') {
		sval.len--;
		sval.data++;
	}

	while(sval.len) {
		size_t old = val;
		val *= 10;
		val += sval.data[0] - '0';
		if(val < old) {
			*s = org;
			return false;
		}
		sval.len--;
		sval.data++;
	}

	if(pval)
		*pval = (int64_t)val * sign;
	return true;
}

static inline bool skip_val_u64(
		strsl_t *s,
		uint64_t *pval)
{
	strsl_t org = *s;
	strsl_t sval;
	uint64_t val = 0;
	if(!skip_int(s, &sval))
		return false;
	else if(sval.data[0] == '-') {
		*s = org;
		return false;
	}
	else if(sval.data[0] == '+') {
		sval.len--;
		sval.data++;
	}

	while(sval.len) {
		size_t old = val;
		val *= 10;
		val += sval.data[0] - '0';
		if(val < old) {
			*s = org;
			return false;
		}
		sval.len--;
		sval.data++;
	}

	if(pval)
		*pval = val;
	return true;
}

static inline bool skip_hexstr(
		strsl_t *s,
		uint8_t *val,
		size_t len) /* in bytes*/
{
	strsl_t tmp = *s;
	while(len) {
		char c;
		uint8_t b;
		if(tmp.len < 2)
			return false;
		c = *tmp.data++;
		if(c >= '0' && c <= '9')
			b = c - '0';
		else if(c >= 'a' && c <= 'f')
			b = c - 'a' + 0x0a;
		else if(c >= 'A' && c <= 'F')
			b = c - 'A' + 0x0a;
		else
			return false;
		b <<= 4;
		c = *tmp.data++;
		if(c >= '0' && c <= '9')
			b |= c - '0';
		else if(c >= 'a' && c <= 'f')
			b |= c - 'a' + 0x0a;
		else if(c >= 'A' && c <= 'F')
			b |= c - 'A' + 0x0a;
		else
			return false;

		*val++ = b;
		len--;
		tmp.len -= 2;
	}
	*s = tmp;
	return true;
}

static inline int skip_nibble(
		strsl_t *s)
{
	char c;
	if(skip_crange(s, '0', '9', &c))
		return c - '0';
	else if(skip_crange(s, 'a', 'f', &c))
		return c - 'a' + 0xa;
	else if(skip_crange(s, 'A', 'F', &c))
		return c - 'A' + 0xa;
	else
		return -1;
}


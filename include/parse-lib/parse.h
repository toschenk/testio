typedef struct parse_num parse_num_t;

enum {
	PARSE_OK,
	PARSE_LINEEND = -20,
	PARSE_INVALID_CHAR,
	PARSE_INCOMPLETE,
	PARSE_INVALID_NUMBASE,
	PARSE_OVERFLOW,
/*	PARSE_ALREADY,
	PARSE_INVALID_MODE,
	PARSE_INVALID_COMMAND,
	PARSE_NO_SUCH_NAME,
	PARSE_BOUNDS,*/
};

enum {
	PARSE_NUM_NULL,
	PARSE_NUM_SINT,
	PARSE_NUM_UINT,
	PARSE_NUM_FLOAT,
};

struct parse_num {
	int type;
	union {
		uint64_t u;
		int64_t s;
		double f;
	};
};

static inline char *put_utf8(
		char *di,
		uint32_t utf32)
{
	if(utf32 < 0x80)
		*di++ = utf32;
	else if(utf32 < 0x800) {
		*di++ = 0xc0 | ((utf32 >>  6) & 0x1f);
		*di++ = 0x80 | ((utf32 >>  0) & 0x3f);
	}
	else if(utf32 < 0x10000) {
		*di++ = 0xe0 | ((utf32 >> 12) & 0x0f);
		*di++ = 0x80 | ((utf32 >>  6) & 0x3f);
		*di++ = 0x80 | ((utf32 >>  0) & 0x3f);
	}
	else {
		*di++ = 0xf0 | ((utf32 >> 18) & 0x0f);
		*di++ = 0x80 | ((utf32 >> 12) & 0x3f);
		*di++ = 0x80 | ((utf32 >>  6) & 0x3f);
		*di++ = 0x80 | ((utf32 >>  0) & 0x3f);
	}
	return di;
}

//TODO is that a skip_val_u64base? maybe not, since we have error return values
//semantics of this function is more like "i expect an integer, parse it or tell me what's wrong"
static inline int parse_u64(
		strsl_t *s,
		unsigned base,
		uint64_t *result)
{
	uint64_t v = 0;
	uint64_t mul;
	char c;
	if(base > 36)
		return PARSE_INVALID_NUMBASE;
	else if(base > 10) {
		char upper_lcase = 'a' + base - 11;
		char upper_ucase = 'A' + base - 11;
		for(;;) {
			uint64_t nv;
			if(skip_crange(s, '0', '9', &c))
				nv = c - '0';
			else if(skip_crange(s, 'a', upper_lcase, &c))
				nv = c - 'a' + 0xa;
			else if(skip_crange(s, 'A', upper_ucase, &c))
				nv = c - 'A' + 0xa;
			else
				break;
			nv += v * base;
			if(nv < v)
				return PARSE_OVERFLOW;
			v = nv;
		}
	}
	else {
		char upper = '0' + base - 1;
		while(skip_crange(s, '0', upper, &c)) {
			uint64_t nv = v * base + c - '0';
			if(nv < v)
				return PARSE_OVERFLOW;
			v = nv;
		}
	}
	if(result)
		*result = v;
	return 0;
}

static inline int parse_num(
		strsl_t *s,
		parse_num_t *num)
{
	int err = PARSE_OK;
	char sign = 0;
	unsigned base = 10;
	ssize_t pos_dot = -1;
	ssize_t pos_exp = -1;

	//TODO for floats, see https://en.cppreference.com/w/cpp/language/floating_literal
	//see also https://github.com/BlankOn/musl/blob/master/src/internal/floatscan.c

	skip_cropt(s, &sign, '+', '-', 0);
	if(skip_char(s, '0')) { /* 0; integer with alternative base; float */
		if(s->len == 0)
			strsl_rewind(s, 1);
		else if(skip_char(s, 'x'))
			base = 16;
		else if(skip_char(s, 'b'))
			base = 2;
		else
			base = 8;
	}
	if(s->len == 0) {
		if(sign)
			return num->type = PARSE_NUM_SINT;
		else
			return num->type = PARSE_NUM_UINT;
		return PARSE_INCOMPLETE;
	}

	/* check, whether we have a float */
	if(base == 10)
		for(size_t i = 0; i < s->len; i++) {
			char c = s->data[i];
			if(c == '.')
				pos_dot = i;
			else if(c == 'e' || c == 'E')
				pos_exp = i;
		}
	else if(base == 16)
		for(size_t i = 0; i < s->len; i++) {
			char c = s->data[i];
			if(c == '.')
				pos_dot = i;
			else if(c == 'p' || c == 'P')
				pos_exp = i;
		}

	if(pos_dot >= 0 || pos_exp >= 0) { /* we have a float */
		assert(!"implement me");
		num->type = PARSE_NUM_FLOAT;
	}
	else { /* we have an (u)int */
		uint64_t v;
		if(sign)
			num->type = PARSE_NUM_SINT;
		else
			num->type = PARSE_NUM_UINT;
		err = parse_u64(s, base, &v);
		if(err) { /* nop */ }
		else if(s->len)
			err = PARSE_INVALID_CHAR;
		else if(sign == '-') {
			int64_t sv = -(int64_t)v;
			if(sv > 0)
				err = PARSE_OVERFLOW;
			else
				num->s = sv;
		}
		else if(sign == '+') {
			int64_t sv = (int64_t)v;
			if(sv < 0)
				err = PARSE_OVERFLOW;
			else
				num->s = sv;
		}
		else
			num->u = v;
	}

	return err;
}


static inline int parse_unescape(
		char *string,
		char quot,
		char esc,
		size_t *plen,
		size_t *errpos)
{
	int ret = 0;
	char *di = string;
	const char *si = string;
	size_t maxlen;
	strsl_t s;

	if(plen)
		maxlen = *plen;
	else
		maxlen = SIZE_MAX;
	maxlen = strnlen(string, maxlen);
	s = strsl_snmk(string, maxlen);
	while(s.len) {
		if(skip_char(&s, esc)) {
			/* copy range between si and s.data-1 to di (if necessary) and advance di */
			char c;
			size_t nmove = s.data - si - 1;
			if(si > di)
				memmove(di, si, nmove);
			di += nmove;

			if(s.len == 0) {
				strsl_rewind(&s, 1);
				ret = PARSE_INCOMPLETE;
				goto error;
			}

			if(skip_char(&s, esc))
				*di++ = esc;
			else if(skip_char(&s, quot))
				*di++ = esc;
			else if(skip_cropt(&s, &c, 'a', 'b', 'e', 'f', 'n', 'r', 't', 'v', '\'', '"', 0))
				switch(c) {
					case 'a': *di++ = '\a'; break;
					case 'b': *di++ = '\b'; break;
					case 'e': *di++ = 0x1b; break;
					case 'f': *di++ = '\f'; break;
					case 'n': *di++ = '\n'; break;
					case 'r': *di++ = '\r'; break;
					case 't': *di++ = '\t'; break;
					case 'v': *di++ = '\v'; break;
					case '\'': *di++ = '\''; break;
					case '"': *di++ = '"'; break;
					default: assert(!"implement me");
				}
			else if(skip_cropt(&s, &c, 'u', 'U', 0) || peek_crange(&s, '0', '3', NULL)) {
				strsl_t val = s;
				uint64_t utf32;
				unsigned base = 16;
				switch(c) {
					case 'u': val.len = 4; break;
					case 'U': val.len = 8; break;
					default: val.len = 3; base = 8; break;
				}
				if(s.len < val.len) {
					strsl_rewind(&s, 2);
					ret = PARSE_INCOMPLETE;
					goto error;
				}
				parse_u64(&val, base, &utf32);
				if(val.len) {
					s.len = s.data + s.len - val.data;
					s.data = val.data;
					ret = PARSE_INVALID_CHAR;
					goto error;
				}
				di = put_utf8(di, utf32);
			}
			else if(skip_char(&s, 'x')) {
				size_t nchar = 0;
				for(;;) {
					int l, h;
					if((h = skip_nibble(&s)) < 0)
						break;
					else if((l = skip_nibble(&s)) < 0) {
						ret = PARSE_INVALID_CHAR;
						goto error;
					}
					*di++ = (h << 4) | l;
					nchar += 2;
				}
				if(nchar == 0) {
					strsl_rewind(&s, 2);
					ret = PARSE_INCOMPLETE;
					goto error;
				}
			}
			else {
				ret = PARSE_INVALID_CHAR;
				goto error;
			}
			si = s.data;
		}
		else
			strsl_fchop(&s);
	}

error:
	if(ret) {
		if(ret == PARSE_INVALID_CHAR && s.len == 0)
			ret = PARSE_INCOMPLETE;
		if(errpos)
			*errpos = s.data - string;
	}
	else {
		size_t nmove = s.data - si;
		size_t total;
		assert(si <= s.data);
		if(si > di)
			memmove(di, si, nmove);
		di += nmove;
		total = di - string;
		if(total < maxlen)
			*di = 0;
		if(plen)
			*plen = total;
	}
	return ret;
}

static inline bool parse_uncomment(
		bool in_block,
		char *str,
		char repl)
{
	enum {
		COMMENT_NONE,
		COMMENT_LINE,
		COMMENT_BLOCK,
		COMMENT_QUOTED,
	};

	int state = in_block ? COMMENT_BLOCK : COMMENT_NONE;
	char delim;
	for(; *str; str++) {
		switch(state) {
			case COMMENT_NONE:
				if(str[0] == '/') {
					if(str[1] == '/' || str[1] == '*') {
						if(str[1] == '*')
							state = COMMENT_BLOCK;
						else
							state = COMMENT_LINE;
						str[0] = repl;
						str[1] = repl;
						str++;
					}
				}
				else if(str[0] == '"' || str[0] == '\'') {
					delim = str[0];
					state = COMMENT_QUOTED;
				}
				break;
			case COMMENT_LINE:
				if(str[0] == '\r' || str[0] == '\n')
					state = COMMENT_NONE;
				else
					str[0] = repl;
				break;
			case COMMENT_BLOCK:
				if(str[0] == '*' && str[1] == '/') {
					str[0] = repl;
					str[1] = repl;
					str++;
					state = COMMENT_NONE;
				}
				else if(str[0] != '\r' && str[0] != '\n')
					str[0] = repl;
				break;
			case COMMENT_QUOTED:
				if(str[0] == '\\') {
					if(str[1] == 0)
						return false;
					str++;
				}
				else if(str[0] == delim)
					state = COMMENT_NONE;
				break;
			default:
				assert(!"unexpected state");
		}
	}
	return state == COMMENT_BLOCK;
}


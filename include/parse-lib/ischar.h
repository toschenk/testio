#pragma once

#ifndef ISCHAR_NOSPC
static inline bool ischar_spc(
		char c)
{
	return c == ' ' || c == '\t' || c == '\n' || c == '\r';
}
#endif

#ifndef ISCHAR_NOSPCLINE
static inline bool ischar_spcl(
		char c)
{
	return c == ' ' || c == '\t';
}
#endif

#ifndef ISCHAR_NONEWLINE
static inline bool ischar_newline(
		char c)
{
	return c == '\r' || c == '\n';
}
#endif


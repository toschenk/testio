static inline bool peek_crange(
		strsl_t *s,
		char from,
		char to,
		char *c)
{
	if(s->len == 0)
		return false;
	else if(*s->data >= from && *s->data <= to) {
		if(c)
			*c = *s->data;
		return true;
	}
	else
		return false;
}

static inline bool peek_char(
		strsl_t *s,
		char c)
{
	if(s->len == 0)
		return false;
	else
		return s->data[0] == c;
}

static inline bool peek_fn(
		strsl_t *s,
		bool (*ischar)(char c),
		char *c)
{
	if(s->len && ischar(*s->data)) {
		if(c)
			*c = *s->data;
		return true;
	}
	else
		return false;
}

static inline bool peek_spc(
		strsl_t *s)
{
	if(s->len == 0)
		return false;
	else
		return ischar_spc(s->data[0]);
}

static inline unsigned int peek_copt(
		strsl_t *s,
		...)
{
	va_list ap;
	if(s->len == 0)
		return 0;
	va_start(ap, s);
	for(unsigned int opt = 1;; opt++) {
		int c = va_arg(ap, int);
		if(!c) {
			va_end(ap);
			return 0;
		}

		if(*s->data == c) {
			va_end(ap);
			return opt;
		}
	}
}

static inline bool peek_ccat(
		strsl_t *s,
		const unsigned char *cats,
		unsigned char cat,
		bool high,
		char *c)
{
	if(s->len && ((*s->data < 0 && high) || (*s->data >= 0 && (cats[(unsigned char)*s->data] & cat)))) {
		if(c)
			*c = *s->data;
		return true;
	}
	else
		return false;
}

static inline bool peek_cfn(
		strsl_t *s,
		bool (*ischar)(char c),
		char *c)
{
	if(s->len && ischar(*s->data)) {
		if(c)
			*c = *s->data;
		return true;
	}
	else
		return false;
}



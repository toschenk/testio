#pragma once

static inline void hexdump(const void *data, size_t bytes) {
	size_t cur;
	size_t i;
	size_t address = 0;
	const unsigned char *base = data;
	while(bytes > 0) {
		cur = bytes < 16 ? bytes : 16;
		printf("%08x  ", (unsigned int)address);
		for(i = 0; i < cur; i++) {
			if(i == 8)
				printf(" ");
			printf("%02x ", base[i]);
		}
		for(; i < 16; i++) {
			if(i == 8)
				printf(" ");
			printf("   ");
		}
		printf(" |");
		for(i = 0; i < cur; i++)
			if(base[i] < 0x20 || base[i] >= 0x7f)
				printf(".");
			else
				printf("%c", base[i]);
		printf("|\n");
		bytes -= cur;
		address += cur;
		base += cur;
	}
}

static inline void hexdump_ext(FILE *out, const char *indent, const void *data, size_t bytes, int flags) {
	size_t cur;
	size_t i;
	size_t address = 0;
	const unsigned char *base = data;
	while(bytes > 0) {
		cur = bytes < 16 ? bytes : 16;
		fprintf(out, "%s%08x  ", indent, (unsigned int)address);
		for(i = 0; i < cur; i++) {
			if(i == 8)
				fprintf(out, " ");
			fprintf(out, "%02x ", base[i]);
		}
		for(; i < 16; i++) {
			if(i == 8)
				fprintf(out, " ");
			fprintf(out, "   ");
		}
		fprintf(out, " |");
		for(i = 0; i < cur; i++)
			if(base[i] < 0x20 || base[i] >= 0x7f)
				fprintf(out, ".");
			else
				fprintf(out, "%c", base[i]);
		fprintf(out, "|\n");
		bytes -= cur;
		address += cur;
		base += cur;
	}
}


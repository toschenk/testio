.PHONY: all
all: testio echo

testio: testio.c
	$(CC) -o $@ $^ -Iinclude

echo: echo.c
	$(CC) -o $@ $^ -Iinclude

clean:
	rm -f testio echo


#include <assert.h>
#include <parse-lib/lib.h>
#include <parse-lib/parse.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <hexdump.h>
#include <stdalign.h>
#include <stddef.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <errno.h>
#include <limits.h>
#include <netdb.h>
#include <poll.h>

#include <oob.h>

#define MIN(X, Y) ((X) < (Y) ? (X) : (Y))
#define AlignUp(OFF, ALIGN) \
		(((uintptr_t)(OFF) + ((ALIGN) - 1)) & ~((ALIGN) - 1))

enum {
	DATASEG_SIZE = 16777216,
	OPSTACK_SIZE = 65536,
	PROGRAM_SIZE = 65536,
	OPCHUNK_SIZE = 32
/*	SYSDICT_SIZE = 256,
	USERDICT_SIZE = 1024,
	DICTSTACK_SIZE = 32,

	NAME_MAXLEN = 10,*/
};

typedef struct op op_t;
typedef struct multistr multistr_t;
typedef struct vm vm_t;
typedef struct filemap filemap_t;
typedef struct dict dict_t;
typedef struct ident ident_t;
typedef struct opchunk opchunk_t;
typedef struct opqueue opqueue_t;

enum {
	OP_NOP,
	OP_DATA,
	OP_SIZE,

	OP_CONNECT,
	OP_DISCONNECT,
	OP_SEND,
	OP_RECV,
	OP_EOS,
	OP_EOS_REMOTE,
	OP_SYNC,
	OP_TIMEOUT,
	OP_HEXDUMP,
	OP_STRDUMP,
};

enum { /* NOTE: keep order synced with sopt below */
	MULTISTR_NONE,
	MULTISTR_RAW,

	MULTISTR_COMMENT,
};

struct multistr {
	int type;
	int line;
	strsl_t indent;
	strsl_t delim; /* begin: characters after '---'; end: characters begore '---' */
	char *data;
	size_t offset;
	const char *literal;
	union {
		struct {
			strsl_t newline;
			strsl_t marker;
			size_t marker_count;
		} raw;
	};
};

struct ident {
	uint64_t id;
};

struct op {
	int opcode;
	int line;
	int col;
	int literal_len;
	const char *literal;
	union {
		struct {
			const void *base;
			size_t size;
			size_t off;
		} data;
		size_t size;
		ident_t name;
	};
};

struct opchunk {
	opchunk_t *next;
	op_t data[OPCHUNK_SIZE];
};

struct opqueue {
	opchunk_t *head;
	opchunk_t *tail;
	size_t head_pos;
	size_t tail_pos;
};

struct dict {
	ident_t key;
	op_t value;
};

struct vm {
	struct sockaddr_in addr;
	int sock;
	uint32_t send_ptr;
	uint32_t recv_ptr;

	opqueue_t send;
	opqueue_t recv;

	opchunk_t *opchunk_unused;

	char *dataseg_base;
	size_t dataseg_tail;
	op_t *opstack_base;
	size_t opstack_top;
	op_t *program_base;
	size_t program_tail;
	size_t program_ip;
};

struct filemap {
	size_t size;
	void *addr;
};


static bool ischar_spcnul(
		char c)
{
	return c == '\t' || c == ' ' || c == 0;
}

#if 0
static bool ischar_name0(
		char c)
{
	return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}

static bool ischar_name(
		char c)
{
	return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || c == '_';
}
#endif

static void *map_readfile(
		const char *filename,
		filemap_t *map)
{
	void *addr;
	struct stat st;
	int fd = open(filename, O_RDONLY);
	memset(map, 0, sizeof(*map));
	if(fd < 0)
		return NULL;
	else if(fstat(fd, &st) < 0 || st.st_size == 0) {
		close(fd);
		return NULL;
	}
	addr = mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	close(fd);
	if(addr) {
		map->addr = addr;
		map->size = st.st_size;
	}
	return addr;
}

static void *dataseg_push(
		vm_t *vm,
		size_t align,
		size_t size)
{
	size_t off = vm->dataseg_tail;
	size_t end;
	uintptr_t begin_ptr = (uintptr_t)(vm->dataseg_base + off);
	if(!align)
		align = alignof(max_align_t);
	begin_ptr = AlignUp(vm->dataseg_base + off, align);
	off = begin_ptr - (uintptr_t)vm->dataseg_base;
	end = off + size;

	if(off >= DATASEG_SIZE || DATASEG_SIZE - off < size)
		return NULL;

	vm->dataseg_tail = end;
	return vm->dataseg_base + off;

}

static void opqueue_push(
		vm_t *vm,
		opqueue_t *queue,
		op_t op)
{
	op_t *dest;
	//printf("PUSH QUEUE: %p\n", queue);
	if(!queue->tail || queue->tail_pos == OPCHUNK_SIZE) {
		opchunk_t *next;
		if(vm->opchunk_unused) {
			next = vm->opchunk_unused;
			vm->opchunk_unused = next;
		}
		else {
			next = malloc(sizeof(*next));
			assert(next);
		}
		next->next = NULL;
		if(queue->tail)
			queue->tail->next = next;
		else
			queue->head = next;
		queue->tail = next;
		queue->tail_pos = 1;
		dest = next->data;
	}
	else
		dest = queue->tail->data + queue->tail_pos++;

	*dest = op;
}

static void opqueue_dump(
		opqueue_t *queue)
{
	size_t front = queue->head_pos;

	for(opchunk_t *chunk = queue->head;; chunk = chunk->next) {
		size_t back;
		if(chunk == queue->tail)
			back = queue->tail_pos;
		else
			back = OPCHUNK_SIZE;
		for(size_t pos = front; pos < back; pos++) {
			op_t *op = chunk->data + pos;
			printf("  token=%.*s\n", (int)op->literal_len, op->literal);

		}
		front = 0;
		if(chunk == queue->tail)
			break;
	}
}

static op_t *opqueue_peek(
		opqueue_t *queue)
{
	static op_t op_null;
	if(queue->head != queue->tail || queue->head_pos < queue->tail_pos)
		return queue->head->data + queue->head_pos;
	else
		return &op_null;
}

static op_t opqueue_poll(
		vm_t *vm,
		opqueue_t *queue)
{
	op_t op = { 0 };
	//printf("POLL QUEUE: %p\n", queue);
	if(queue->head != queue->tail) {
		op = queue->head->data[queue->head_pos++];
		if(queue->head_pos == OPCHUNK_SIZE) {
			opchunk_t *prev = queue->head;
			queue->head = prev->next;
			queue->head_pos = 0;
			prev->next = vm->opchunk_unused;
			vm->opchunk_unused = prev;
		}
	}
	else if(queue->head_pos < queue->tail_pos) {
		op = queue->head->data[queue->head_pos++];
		if(queue->head_pos == queue->tail_pos) {
			opchunk_t *prev = queue->head;
			memset(queue, 0, sizeof(*queue));
			prev->next = vm->opchunk_unused;
			vm->opchunk_unused = prev;
		}
	}
	return op;
}

static bool opqueue_isempty(
		opqueue_t *queue)
{
	return queue->tail == queue->head && queue->tail_pos == queue->head_pos;
}

#if 0
static bool ident_isvalid(
		ident_t ident)
{
	return ident.id != 0;
}

static ident_t parse_ident(
		strsl_t name)
{
	strsl_t tmp = name;
	ident_t ident = { 0 };
	if(tmp.len > 0 && name.len <= NAME_MAXLEN && skip_fnseq0(&tmp, ischar_name0, ischar_name, NULL) && tmp.len == 0) {
		int shift = 58;
		for(size_t i = 0; i < name.len; i++) {
			char c = name.data[i];
			uint64_t v;
			if(c >= '0' && c <= '9')
				v = c - '0' + 1;
			else if(c >= 'a' && c <= 'z')
				v = c - 'a' + 11;
			else if(c >= 'A' && c <= 'Z')
				v = c - 'A' + 37;
			else if(c == '_')
				v = 63;
			else {
				assert(0);
			}
			ident.id |= v << shift;
			shift -= 6;
		}
	}
	return ident;
}
#endif

static int load_file(
		vm_t *vm,
		const char *filename)
{
	filemap_t map;
	char *source = map_readfile(filename, &map);
	int lineno = 0;
	int err = -1;
	multistr_t multistr = { 0 };
	

	if(!source) {
		fprintf(stderr, "error loading testio file\n");
		return -1;
	}

	for(strsl_t content = strsl_snmk(source, map.size); content.len > 0;) {
		strsl_t line;
		const char *line_base;

		skip_line(&content, &line);
		lineno++;
		line_base = line.data;
		//printf("CHECK: %zu %zu\n", content.len, line.len);

		strsl_rtrim(&line);

		if(multistr.type) { /* in multistring? */
			strsl_t tmp;
			if(line.len > 0 && !skip_strsl(&line, multistr.indent)) {
				fprintf(stderr, "error in line %d: invalid indentation in multiline string\n", lineno);
				goto fail;
			}

			tmp = line;
			if(skip_strsl(&line, multistr.delim) && skip_str(&line, "---")) { /* end of string */
				if(multistr.type != MULTISTR_COMMENT) {
					size_t total;
					op_t *op;
					total = vm->dataseg_base + vm->dataseg_tail - multistr.data;
					assert(total == multistr.offset);
					switch(multistr.type) {
						case MULTISTR_RAW:
							if(multistr.raw.marker.len > 0) {
								if(vm->program_tail == PROGRAM_SIZE) {
									fprintf(stderr, "error in line %d: program too large\n", lineno);
									goto fail;
								}
								op = vm->program_base + vm->program_tail++;
								op->line = multistr.line;
								op->col = 0;
								op->opcode = OP_SIZE;
								op->size = multistr.raw.marker_count;
							}
							break;
						default:
							break;
					}
					if(vm->program_tail == PROGRAM_SIZE) {
						fprintf(stderr, "error in line %d: program too large\n", lineno);
						goto fail;
					}
					op = vm->program_base + vm->program_tail++;
					op->line = multistr.line;
					op->literal = multistr.literal;
					op->literal_len = line.data - multistr.literal;
					op->col = 0;
					op->opcode = OP_DATA;
					op->data.base = multistr.data;
					op->data.size = total;
					//hexdump(op->data.base, op->data.size);
				}
				multistr.type = MULTISTR_NONE;
			}
			else { /* append line */
				line = tmp;
				switch(multistr.type) {
					case MULTISTR_COMMENT:
						break;

					case MULTISTR_RAW:
					{
						strsl_t frag;
						char *dest;
						while(skip_toksl(&line, multistr.raw.marker, &frag)) {
							op_t *op;
							dest = dataseg_push(vm, 1, frag.len);
							if(!dest) {
								fprintf(stderr, "error in line %d: cannot allocate multistring line\n", lineno);
								goto fail;
							}
							else if(vm->program_tail == PROGRAM_SIZE) {
								fprintf(stderr, "error in line %d: program too large\n", lineno);
								goto fail;
							}
							multistr.offset += frag.len;
							multistr.raw.marker_count++;
							op = vm->program_base + vm->program_tail++;
							op->line = lineno;
							op->col = (frag.data + frag.len) - line_base;
							op->opcode = OP_SIZE;
							op->size = multistr.offset;
							memcpy(dest, frag.data, frag.len);
						}
						dest = dataseg_push(vm, 1, line.len + multistr.raw.newline.len);
						if(!dest) {
							fprintf(stderr, "error in line %d: cannot allocate multistring line\n", lineno);
							goto fail;
						}
						memcpy(dest, line.data, line.len);
						memcpy(dest + line.len, multistr.raw.newline.data, multistr.raw.newline.len);
						multistr.offset += line.len + multistr.raw.newline.len;
					} break;

					default:
						assert(0);
						break;
				}
			}
		}
		else { /* start multiline string? (only at line begin) */
			strsl_t indent;
			skip_spcr(&line, &indent);

			if(skip_str(&line, "---")) { /* multistring start? */
				memset(&multistr, 0, sizeof(multistr));
				multistr.line = lineno;
				multistr.indent = indent;
				multistr.literal = line_base;
				if(skip_char(&line, '/')) {
					multistr.type = MULTISTR_COMMENT;
					multistr.delim = strsl_smk("/");
				}
				else {
					skip_tok(&line, ' ', &multistr.delim);
					skip_spc(&line);
					multistr.type = skip_sopt(&line, "raw", NULL); /* NOTE: keep order in sync with MULTISTR_x enum! */
					switch(multistr.type) {
						case MULTISTR_RAW: /* raw string: arguments: ( newline_sequence, [ offset_marker_sequence ] ) */
						{
							strsl_t newline = { 0 };
							bool has_escape;

							/* newline sequence argument */
							skip_spc(&line);
							if(!skip_fnquoted(&line, ischar_spcnul, '\\', &newline, &has_escape)) {
								fprintf(stderr, "error in line %d: invalid characters in newline sequence\n", lineno);
								goto fail;
							}
							else if(newline.len == 0) {
								fprintf(stderr, "error in line %d: missing newline sequence for raw multiline string\n", lineno);
								goto fail;
							}
							else if(has_escape) {
								void *dup = dataseg_push(vm, 1, newline.len);
								if(!dup) {
									fprintf(stderr, "error in line %d: cannot allocate raw string newline chars\n", lineno);
									goto fail;
								}
								memcpy(dup, newline.data, newline.len);
								newline.data = dup;
								if(parse_unescape(dup, ' ', '\\', &newline.len, NULL)) {
									fprintf(stderr, "error in line %d: invalid raw string newline sequence\n", lineno);
									goto fail;
								}
							}
							multistr.raw.newline = newline;

							/* optional offset marker argument */
							skip_spc(&line);
							if(line.len > 0) {
								if(!skip_tok(&line, ' ', &multistr.raw.marker)) {
									multistr.raw.marker = line;
									line.len = 0;
								}
							}

						} break;

						default:
							fprintf(stderr, "error in line %d: unknown multiline string type\n", lineno);
							goto fail;
					}
					if(line.len > 0) {
						fprintf(stderr, "error in line %d: unexpected characters after multiline string begin\n", lineno);
						goto fail;
					}
					multistr.data = vm->dataseg_base + vm->dataseg_tail;
				}
			}
		}

		/* not (any longer) in multistring */
		if(!multistr.type) {
			/* parse tokens in line */
			skip_spc(&line);
			while(line.len > 0) {
				strsl_t val;
				bool has_escape;
				op_t *op;
				const char *token_base = line.data;

				if(skip_char(&line, '#'))
					line.len = 0;
				else if(skip_char(&line, '"')) { /* string literal? */
					if(skip_quoted(&line, '"', '\\', &val, &has_escape)) {
						size_t size = val.len;
						void *dup = dataseg_push(vm, 1, size);
						if(!dup) {
							fprintf(stderr, "error in line %d: cannot allocate string\n", lineno);
							goto fail;
						}

						memcpy(dup, val.data, size);
						if(has_escape && parse_unescape(dup, '"', '\\', &size, NULL)) {
							fprintf(stderr, "error in line %d: invalid string characters\n", lineno);
							goto fail;
						}

						if(vm->program_tail == PROGRAM_SIZE) {
							fprintf(stderr, "error in line %d: program too large\n", lineno);
							goto fail;
						}
						op = vm->program_base + vm->program_tail++;
						op->literal = token_base;
						op->literal_len = line.data - token_base;
						op->line = lineno;
						op->col = val.data - line_base;
						op->opcode = OP_DATA;
						op->data.base = dup;
						op->data.size = size;
					}
					else {
						fprintf(stderr, "error in line %d: unterminated string\n", lineno);
						goto fail;
					}
				}
				else { /* token which spans to next space/line end */
					strsl_t token;
					if(vm->program_tail == PROGRAM_SIZE) {
						fprintf(stderr, "error in line %d: program too large\n", lineno);
						goto fail;
					}
					op = vm->program_base + vm->program_tail++;
					op->line = lineno;
					op->col = token.data - line_base;

					if(!skip_fntok(&line, ischar_spc, &token)) {
						token = line;
						line.len = 0;
					}
					op->literal = token.data;
					op->literal_len = token.len;

					//TODO implement a mechanism to wait for eos from server. this may also shutdown our socket.
					if(!strsl_scmp(token, "connect"))
						op->opcode = OP_CONNECT;
					else if(!strsl_scmp(token, "disconnect"))
						op->opcode = OP_DISCONNECT;
					else if(!strsl_scmp(token, "send"))
						op->opcode = OP_SEND;
					else if(!strsl_scmp(token, "recv"))
						op->opcode = OP_RECV;
					else if(!strsl_scmp(token, "eos"))
						op->opcode = OP_EOS;
					else if(!strsl_scmp(token, "eos.remote"))
						op->opcode = OP_EOS_REMOTE;
					else if(!strsl_scmp(token, "sync"))
						op->opcode = OP_SYNC;
					else if(!strsl_scmp(token, "timeout"))
						op->opcode = OP_TIMEOUT;
					else if(!strsl_scmp(token, "hexdump"))
						op->opcode = OP_HEXDUMP;
					else if(!strsl_scmp(token, "strdump"))
						op->opcode = OP_STRDUMP;
					else {
						fprintf(stderr, "error in line %d: unexpected token '%.*s'\n", lineno, (int)token.len, token.data);
						goto fail;
					}
				}
				skip_spc(&line);
			}
		}
		skip_char(&content, '\r');
		skip_char(&content, '\n');
	}
	err = 0;

fail:
	//platform_unmap_file(&map);
	return err;
}

static int exec_io(
		vm_t *vm)
{
	static char buffer[65536];
	if(vm->sock >= 0) {
		op_t *op;
		size_t n;
		ssize_t ret;
		const char *base;
		struct pollfd pfd = { .fd = vm->sock, .events = POLLIN | POLLOUT };
		int nevents;

		nevents = poll(&pfd, 1, 1000);
		if(nevents < 0) {
			fprintf(stderr, "error polling: %s\n", strerror(errno));
			return -1;
		}
		//printf("poll: %d\n", nevents);

		if(pfd.revents & POLLOUT) {
			op = opqueue_peek(&vm->send);
			switch(op->opcode) {
				case OP_NOP:
					break;
				case OP_DATA:
					n = MIN(op->data.size - op->data.off, SSIZE_MAX);
					base = (const char*)op->data.base + op->data.off;
					assert(n > 0);
					ret = send(vm->sock, base, n, 0);
					fprintf(stderr, "sent: %zd\n", ret);
					if(ret < 0) {
						if(errno != EINTR && errno != EAGAIN && errno != EWOULDBLOCK) {
							fprintf(stderr, "error in line %d: sending data failed: %s\n", op->line, strerror(errno));
							return -1;
						}
					}
					else if((size_t)ret == n)
						opqueue_poll(vm, &vm->send);
					else
						op->data.off += ret;
					break;
				case OP_TIMEOUT:
					assert(!"unimplemented");
					break;
				case OP_EOS:
					if(shutdown(vm->sock, SHUT_WR) < 0) {
						fprintf(stderr, "error shutting down socket: %s\n", strerror(errno));
						return -1;
					}
					opqueue_poll(vm, &vm->send);
					break;
				case OP_EOS_REMOTE:
				{
					char msg;
					uint32_t ptr = vm->send_ptr;
					ptr = htobe32(ptr);
					msg = OOB_EOS;
					ret = send(vm->sock, &msg, 1, MSG_OOB);
					if(ret != 1) {
						fprintf(stderr, "error sending eos remote message to server: %s\n", strerror(errno));
						return -1;
					}
					opqueue_poll(vm, &vm->send);
				} break;
				default:
					assert(0);
			}
		}

		if(pfd.revents & POLLIN) {
			op = opqueue_peek(&vm->recv);
			switch(op->opcode) {
				case OP_NOP:
					break;
				case OP_DATA:
					n = MIN(op->data.size - op->data.off, sizeof(buffer));
					base = (const char*)op->data.base + op->data.off;
					assert(n > 0);
					printf("recv: receiving data, maxsize=%zu...\n", n);
					ret = recv(vm->sock, buffer, n, 0);
					//fprintf(stderr, "  result: %zd\n", ret);
					//ret = recv(vm->sock, (const char*)op->data.base + op->data.off, n, 0);
					if(ret < 0) {
						if(errno != EINTR && errno != EAGAIN && errno != EWOULDBLOCK) {
							fprintf(stderr, "error in line %d: receiving data failed: %s\n", op->line, strerror(errno));
							return -1;
						}
						ret = 0;
					}
					else if(ret == 0) {
						fprintf(stderr, "end of stream while receiving data\n");
						return -1;
					}

					fprintf(stderr, "asserting received data from line %d, off=%zu size=%zd: ", op->line, op->data.off, ret);
					if(memcmp(buffer, base, ret)) {
						fprintf(stderr, "failed\n");
						fprintf(stderr, "--- expected off=%zu\n", op->data.off);
						hexdump(base, ret);
						fprintf(stderr, "--- actual\n");
						hexdump(buffer, ret);
						fprintf(stderr, "---\n");
						return -1;
					}
					fprintf(stderr, "ok\n");

					if((size_t)ret == n)
						opqueue_poll(vm, &vm->recv);
					else
						op->data.off += ret;
					break;
				case OP_HEXDUMP:
				case OP_STRDUMP:
					n = sizeof(buffer);
					printf("recv: receiving data to dump... size=%zu\n", n);
					ret = recv(vm->sock, buffer, n, 0);
					if(ret < 0) {
						if(errno != EINTR && errno != EAGAIN && errno != EWOULDBLOCK) {
							fprintf(stderr, "error in line %d: receiving data failed: %s\n", op->line, strerror(errno));
							return -1;
						}
						ret = 0;
					}
					else if(ret == 0) {
						opqueue_poll(vm, &vm->recv);
						return 0;
					}
					if(op->opcode == OP_HEXDUMP) {
						fprintf(stderr, "--- hexdump size=%zd\n", ret);
						hexdump(buffer, ret);
					}
					else if(op->opcode == OP_STRDUMP) {
						strsl_t content = strsl_snmk(buffer, ret);
						op_t newline;
						if(vm->opstack_top == 0) {
							fprintf(stderr, "error in line %d: newline operand expected for dumpstr\n", op->line);
							return -1;
						}
						newline = vm->opstack_base[--vm->opstack_top];
						if(newline.opcode != OP_DATA) {
							fprintf(stderr, "error in line %d: newline operand expected to be of type data for dumpstr\n", newline.line);
							return -1;
						}
						fprintf(stderr, "---: raw %.*s\n", newline.literal_len - 2, newline.literal + 1); //TODO NOTE: this is hacky... removing the quotes from the original literal is not what is the best solution, but the quickest ;)
						while(content.len > 0) {
							strsl_t line;
							skip_line(&content, &line);
							fprintf(stderr, "%.*s\n", (int)line.len, line.data);
							skip_char(&content, '\r');
							skip_char(&content, '\n');
						}
						fprintf(stderr, ":---\n");
					}
					else {
						assert(0);
					}
					break;
				case OP_EOS:
				case OP_EOS_REMOTE:
					ret = recv(vm->sock, buffer, sizeof(buffer), 0);
					if(ret < 0) {
						fprintf(stderr, "error receiving eos from socket: %s\n", strerror(errno));
						return -1;
					}
					else if(ret != 0) {
						fprintf(stderr, "error in line %d: eos expected but %zd bytes of data received instead\n", op->line, ret);
						hexdump(buffer, ret);
						return -1;
					}
					opqueue_poll(vm, &vm->recv);
					break;
				default:
					assert(0);
			}
		}
	}

	return 0;
}


static int exec_step(
		vm_t *vm)
{
	op_t *insn = vm->program_base + vm->program_ip;
	//printf("IP: %zu %zu\n", vm->program_ip, vm->program_tail);
	if(vm->program_ip == vm->program_tail)
		return 1;
	//printf("exec: ip=%zu opcode=%d '%.*s'\n", vm->program_ip, insn->opcode, insn->literal_len, insn->literal);
	switch(insn->opcode) {
		case OP_NOP:
			break;
		case OP_DATA:
		case OP_SIZE:
			if(vm->opstack_top == OPSTACK_SIZE) {
				fprintf(stderr, "error in line %d: operand stack overflow\n", insn->line);
				return -1;
			}
			vm->opstack_base[vm->opstack_top++] = *insn;
			break;
		case OP_CONNECT:
			if(vm->sock >= 0) {
				fprintf(stderr, "error in line %d: cannot connect, connection already active\n", insn->line);
				return -1;
			}
			vm->sock = socket(AF_INET, SOCK_STREAM, 0);
			if(vm->sock < 0) {
				fprintf(stderr, "error in line %d: error creating socket: %s\n", insn->line, strerror(errno));
				return -1;
			}
			else if(connect(vm->sock, (struct sockaddr*)&vm->addr, sizeof(vm->addr)) < 0) {
				fprintf(stderr, "error in line %d: error connecting: %s\n", insn->line, strerror(errno));
				return -1;
			}
			break;
		case OP_DISCONNECT:
			if(vm->sock < 0) {
				fprintf(stderr, "error in line %d: cannot disconnect, no active connection\n", insn->line);
				return -1;
			}
			close(vm->sock);
			vm->sock = -1;
			break;
		case OP_SEND:
		case OP_RECV:
		{
			op_t op;
			if(vm->opstack_top == 0) {
				fprintf(stderr, "error in line %d: operand stack underflow\n", insn->line);
				return -1;
			}
			op = vm->opstack_base[--vm->opstack_top];
			if(op.opcode == OP_DATA) {
				op.data.off = 0;
			}
			else {
				fprintf(stderr, "error in line %d: data operand expected for send/recv\n", insn->line);
				return -1;
			}
			if(insn->opcode == OP_SEND)
				opqueue_push(vm, &vm->send, op);
			else if(insn->opcode == OP_RECV)
				opqueue_push(vm, &vm->recv, op);
			else {
				assert(0);
			}
		} break;
		case OP_EOS:
		case OP_EOS_REMOTE:
			opqueue_push(vm, &vm->send, *insn);
			opqueue_push(vm, &vm->recv, *insn);
			break;
		case OP_TIMEOUT:
			opqueue_push(vm, &vm->send, *insn);
			break;
		case OP_HEXDUMP:
		case OP_STRDUMP:
			opqueue_push(vm, &vm->recv, *insn);
			break;
		case OP_SYNC:
#if 0
			{
				static bool dumped = false;
				if(!dumped) {
					printf("SEND QUEUE:\n");
					opqueue_dump(&vm->send);
					printf("RECV QUEUE:\n");
					opqueue_dump(&vm->recv);
					dumped = true;
				}
			}
#endif
			while(!opqueue_isempty(&vm->send) || !opqueue_isempty(&vm->recv)) {
				if(exec_io(vm) < 0)
					return -1;
			}
			break;
		default:
			assert(0);
	}
	vm->program_ip++;
	return 0;
}

int main(
		int argn,
		char **argv)
{
	static char mem_dataseg[DATASEG_SIZE];
	static op_t mem_opstack[OPSTACK_SIZE];
	static op_t mem_program[PROGRAM_SIZE];
/*	static dict_t *mem_dictstack[DICTSTACK_SIZE];
	static dict_t mem_sysdict[SYSDICT_SIZE];
	static dict_t mem_userdict[USERDICT_SIZE];*/

	vm_t vm = { .dataseg_base = mem_dataseg, .program_base = mem_program, .opstack_base = mem_opstack };
	const char *arg_host;
	const char *arg_service;
	const char *arg_filename;
	struct addrinfo *addrinfo;

	if(argn != 4 && argn != 2) {
		fprintf(stderr, "Usage: %s <testio-script> [ <host> <service> ]\n", argv[0]);
		return 1;
	}
	arg_filename = argv[1];
	if(argn == 2) {
		arg_host = "127.0.0.1";
		arg_service = "9090";
	}
	else {
		arg_host = argv[2];
		arg_service = argv[3];
	}

	if(getaddrinfo(arg_host, arg_service, NULL, &addrinfo) < 0) {
		fprintf(stderr, "Error resolving address for given service.\n");
		return 1;
	}

	for(struct addrinfo *it = addrinfo; it; it = it->ai_next) {
		if(it->ai_family == PF_INET) {
			assert(it->ai_addrlen == sizeof(vm.addr));
			memcpy(&vm.addr, it->ai_addr, sizeof(vm.addr));
			vm.sock = -1;
			break;
		}
	}
	if(vm.sock == 0) {
		fprintf(stderr, "Cannot find IPv4 address for given service.\n");
		return 1;
	}
/*	vm.addr.sin_addr.s_addr = inet_addr(arg_ipaddr);
	vm.addr.sin_family = AF_INET;
	vm.addr.sin_port = htons(arg_port);*/

	if(load_file(&vm, arg_filename) < 0) {
		fprintf(stderr, "Error loading script file\n");
		return 1;
	}
	for(;;) {
		switch(exec_step(&vm)) {
			case 0:
				break;
			case 1:
				return 0;
			default:
				return 1;
		}
	}
}



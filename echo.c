#include <sys/socket.h>
#include <arpa/inet.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <netdb.h>
#include <assert.h>
#include <string.h>
#include <hexdump.h>

#include <oob.h>

int main(
		int argn,
		char **argv)
{
	const char *arg_host;
	const char *arg_service;
	struct addrinfo *addrinfo;
	struct sockaddr_in addr;
	int sock;
	int y = 1;

	if(argn != 3 && argn != 1) {
		fprintf(stderr, "Usage: %s [ <host> <service> ]\n", argv[0]);
		return 1;
	}
	if(argn == 1) {
		arg_host = "127.0.0.1";
		arg_service = "9090";
	}
	else {
		arg_host = argv[1];
		arg_service = argv[2];
	}

	if(getaddrinfo(arg_host, arg_service, NULL, &addrinfo) < 0) {
		fprintf(stderr, "Error resolving address for given service.\n");
		return 1;
	}

	for(struct addrinfo *it = addrinfo; it; it = it->ai_next)
		if(it->ai_family == PF_INET) {
			assert(it->ai_addrlen == sizeof(addr));
			memcpy(&addr, it->ai_addr, sizeof(addr));
			goto addr_found;
		}
	fprintf(stderr, "Cannot find IPv4 address for given service.\n");
	return 1;

addr_found:
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if(sock < 0) {
		fprintf(stderr, "Error creating socket: %s\n", strerror(errno));
		return 1;
	}
	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &y, sizeof(int));
	if(bind(sock, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
		fprintf(stderr, "Error binding socket: %s\n", strerror(errno));
		return 1;
	}
	else if(listen(sock, 1) < 0) {
		fprintf(stderr, "Error listening on socket: %s\n", strerror(errno));
		return 1;
	}
	
	for(;;) {
		struct sockaddr_in peer_addr;
		socklen_t peer_addrlen;
		int peer;
		fprintf(stderr, "waiting for connection...\n");
		peer = accept(sock, (struct sockaddr*)&peer_addr, &peer_addrlen);
		if(peer < 0) {
			fprintf(stderr, "Error accepting: %s\n", strerror(errno));
			continue;
		}

		for(;;) {
			static char buffer[65536];
			char *si = buffer;
			ssize_t n;
			char msg = OOB_NOP;
			fprintf(stderr, "recv: waiting for data...\n");
			n = recv(peer, &msg, 1, MSG_OOB);
			if(n > 0) {
				switch(msg) {
					case OOB_NOP:
						break;
					case OOB_EOS:
						if(shutdown(peer, SHUT_WR) < 0) {
							fprintf(stderr, "Error shuttong down socket: %s\n", strerror(errno));
							goto disconnect;
						}
						break;
				}
			}
			n = recv(peer, buffer, sizeof(buffer), 0);
			if(n < 0) {
				if(errno != EINTR && errno != EWOULDBLOCK && errno != EAGAIN) {
					fprintf(stderr, "Error receiving data: %s. Closing connection.\n", strerror(errno));
					break;
				}
				n = 0;
			}
			else if(n == 0)
				break;
			fprintf(stderr, "recv: %zd bytes\n", n);

			while(n > 0) {
				ssize_t nout = send(peer, si, n, 0);
				if(nout < 0) {
					fprintf(stderr, "Error sending data: %s. Closing connection.\n", strerror(errno));
					break;
				}
				n -= nout;
				si += nout;
			}
		}

disconnect:
		fprintf(stderr, "closing connection\n");
		close(peer);
	}

	return 0;
}

